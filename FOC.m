%%FOC

function F=FOC(va,vb,vc,p)
qa=(exp(va-p(1)))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)));

qb=(exp(vb-p(2)))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)));

qc=(exp(vc-p(3)))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)));

FOC1=qa-qa*(1-qa)*p(1);
FOC2=qb-qb*(1-qb)*p(2);
FOC3=qc-qc*(1-qc)*p(3);
F=[FOC1; FOC2; FOC3];
end

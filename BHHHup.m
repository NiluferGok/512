function betaup = BHHHup(X,y,beta)

gradient= zeros(6,601);

for i=1:601
    gradient(:,i) = grad(X(i,:),y(i),beta);   
end

J = zeros(6);

for i = 1:601
   J = gradient(:,i)*gradient(:,i)' + J; 
end

betaup = beta + J\sum(gradient')';
    
    function g = grad(xi,yi,beta)
        g = -exp(xi*beta)*xi' + yi*xi';
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nilufer Gok -----Homework 4
% I completed this homework with help from Garima
% I could not maximize the likelihood function using monte carlo methods



clc
clear

load ('../../../512a/1_semester/homework/hw4data.mat')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Gaussian Quadrature with 5 nodes
likelihood1 = lhd(0.1,1,0,data, 5); %GQ=5
likelihood1
%Gaussian Quadrature with 10 nodes
likelihood2=lhd(0.1,1,0,data, 10); %GQ=10
likelihood2
%Monte Carlo with 20 nodes
likelihood3=lhdmontecarlo(0.1,1,0,data, 20);%GQ=20
likelihood3



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%Maximum Likelihood when u=0, sigmau=0 and sigmaub=0

init = [1 1 0]';        % initial guess
%Gaussian Quadrature with 5 nodes
maxlike = @(init) lhd(init(1),init(2),init(3),data, 5);
init1=init
argmax1 = fminunc(maxlike,init)
maxlike1=maxlike(argmax1)

%Gaussian Quadrature with 10 nodes
maxlike = @(init) lhd(init(1),init(2),init(3),data, 10);

init2=init
argmax2 = fminunc(maxlike,init)
maxlike2=maxlike(argmax2)

%Monte Carlo with 20 nodes

 
maxlike = @(init) lhdmontecarlo(init(1),init(2),init(3),data, 20);

init3=init
argmax3 = fminunc(maxlike,init)
maxlike3=maxlike(argmax3)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%Maximum Likelihood using all parameters and Monte Carlo method

init = [1 1 1 1 0 0]';        % initial guess

%Monte Carlo with 20 nodes

maxlike = @(init) lhdmontecarlomax(init(1),init(2),init(3),init(4),init(5),init(6),data, 20);

init4=init
argmax4 = fminunc(maxlike,init)
maxlife4=maxlike(argmax4)
% this thing does not work because you redraw your MC draw every evaluation
% that confuses the algorithm and it cannot proceede or make a step. 

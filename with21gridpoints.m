clear all;
close all;

% Parameter values

delta=0.95; % discount factor
 
 Z=21;              % grid number for shocks
 sigma=0.1;       % std of error term in AR(1) process
 mu=0;           % unconditional mean 
 ro=0.5;       % AR(1) parameter 
 p0=0.5; % initial level of prices
 
 

% GRID FOR CAPITAL

% step 1: using Tauchen method to find the grid points for the erogodic set
% of In(k)

N=1000;
varu=(sigma)^2;
varp=(1-ro^2)*varu;

%stde=sqrt(vare);

mux=log(delta)+varp/2;
varx=(ro^2)*varp/(1-ro^2);
logx=mux-4*sqrt(varx):8*sqrt(varx)/(N-1):mux+4*sqrt(varx);

% step 2: approximate the expected value of capital by using the grid points
xx=exp(logx);
xxm=mean(xx);
% why sis you need this transformation? initial value was supposed to be
% between 0 and 100, not between 0 and 100 times 

% step 3: find the grid point for k in between 10% below and 10% above the
% expected value of k
N=100;
xhigh=100;
xlow=0;
xdif=xhigh-xlow;

x=xlow*xxm:xdif*xxm/N:(xhigh-(1-xlow)/N)*xxm;

clear vara varp mux varx logx xx xxm; 

% GRID FOR PRODUCTIVITY SHOCKS

% grid for shock by using Tauchen's method for finite state Markov
% approximation

[prob,grid]=tauchen2(Z,mu,ro,sigma);
disp(['The dimensions of prob are ' num2str(size(prob)) ])
disp(['The dimensions of grid are ' num2str(size(grid)) ])
% VALUE FUNCTION ITERATION

vinitial=zeros(N,Z);
vrevised=zeros(N,Z);
decision=zeros(N,Z);
  
leave=kron(ones(1,Z),x');
disp(['The dimensions of invest  are ' num2str(size(leave)) ])
ONE=ones(N,1);
 
%iteration


diff=1;

while diff>0.001
    
    Ev=vinitial*prob';   % find the expected value of value function
      
    for i=1:N           % for each k, find the optimal decision rule
      ci=(kron(exp(grid),x(i)*ONE))-exp(grid).*leave-(0.2)*(x(i)*ONE-leave).^(1.5);
      % there is a mistake here, this code does not run. 
      ci(ci<0)=NaN;
      ci(isnan(ci))=-Inf;
     % disp(['The dimensions of ci are ' num2str(size(ci)) ])
      [vrevised(i,:),decision(i,:)]=max(ci+delta*Ev);
     % disp(['In the loop vreviesed are ' num2str(size(vrevised)) ])
    end
   % disp(['The dimensions of vreviesed are ' num2str(size(vrevised)) ])
     diff=norm(vrevised-vinitial)/norm(vrevised)
     vinitial=vrevised;
    
end

% compute decision rule

plot(x,vrevised(:,8))
hold on
xlabel('current capital');
ylabel('value');
title('value function')
%disp('PRESS ANY KEY TO CONTINUE')
%pause
%close
plot(x,vrevised(:,11))
hold on
xlabel('current capital');
ylabel('value');
plot(x,vrevised(:,14))
xlabel('current capital');
ylabel('value');
legend('p=0.9','p=1','p=1.1')
hold off
% compute decision rule

derule=zeros(N,Z);

for i=1:Z
    derule(:,i)=x(decision(:,i))';
end


plot(x,derule)
hold on
plot(x,x)
xlabel('current capital');
ylabel('next period capital');
title('optimal investment decision')
hold off



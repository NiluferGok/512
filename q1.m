function [ qa qb qc q0] = q1(va, vb, vc, pa, pb, pc)

qa=(exp(va-pa))/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));

qb=(exp(vb-pb))/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));

qc=(exp(vc-pc))/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));

q0=1/(1+exp(va-pa)+exp(vb-pb)+exp(vc-pc));

end


function Q = Q4( va,vb,vc,p );
qa=(exp(va-p(1)))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)));

qb=(exp(vb-p(2)))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)));

qc=(exp(vc-p(3)))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-p(3)));

Q=[inv(1-qa);inv(1-qb);inv(1-qc)];

end


function betahatb = BHHH(X,y,beta0,tol)

err = 10^10;
beta1 = beta0;

while err>=tol
   beta2 = BHHHup(X,y,beta1);
   err = max(abs(beta2-beta1));
   beta1 = beta2;
end

betahatb=beta1;
end
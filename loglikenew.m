function [ lnL, dlnL] = loglikenew(X,y,beta0 )

n=601;
k=6;
lnL = -sum(-exp(X*beta0) + y.*(X*beta0) - log(factorial(y))); %601x1

if nargout>1
    dlnL= -(sum(diag(-exp(X*beta0))*X+ diag(y)*X))';%%gradient
end
 
end


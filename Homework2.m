clear
clc
%%
%Question1 
[ qa, qb, qc,q0 ] = q1( -1,-1,-1,1,1,1)

%%
%Question2
v=[-1 -1 -1];                       %Fixing all the values
system=@(p) FOC(v(1),v(2),v(3),p);   %take values as given, function of p only
initial=[1; 1; 1];           %initial guess

tic; 
broydsolution = broyden(system,initial)';
btime=toc;
display(broydsolution)
display(btime)


initial2=[0;0;0];
tic; 
broydsolution = broyden(system,initial2)';
btime=toc;
display(broydsolution)
display(btime)

initial3=[0;1;2];
tic; 
broydsolution = broyden(system,initial3)';
btime=toc;
display(broydsolution)
display(btime)

initial4=[3;2;1];
tic; 
broydsolution = broyden(system,initial4)';
btime=toc;
display(broydsolution)
display(btime)

%%
%Question3

t=1e-10; %%tolerance level
pit= initial;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
y= q3(-1,-1,-1,pit);
   dif=abs(pit-y);
   pit = y;
   end
gstime1=toc
gssolution1 = pit'

t=1e-10; %%tolerance level
pit2= initial2;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
  y= q3(-1,-1,-1,pit2);
   dif=abs(pit2-y);
   pit2 = y;
   end
gstime2=toc
gssolution2 = pit2'

t=1e-10; %%tolerance level
pit3= initial3;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
  y= q3(-1,-1,-1,pit3);
   dif=abs(pit3-y);
   pit3 = y;
   end
gstime3=toc
gssolution3 = pit3'

t=1e-10; %%tolerance level
pit4= initial4;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
   y= q3(-1,-1,-1,pit4);
   dif=abs(pit4-y);
   pit4 = y;
      end
gstime4=toc
gssolution4 = pit4'

%Broyden method seems to converge faster than this one%

%%
%Question4
t=1e-10; %%tolerance level
pit= initial;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
   q= Q4(-1,-1,-1,pit);
   dif=abs(pit-q);
   pit = q;
      end
ntime1=toc
nsolution1 = pit'


t=1e-10; %%tolerance level
pit2= initial2;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
   q= Q4(-1,-1,-1,pit2);
   dif=abs(pit2-q);
   pit2 = q;
      end
ntime2=toc
nsolution2 = pit2'

t=1e-10; %%tolerance level
pit3= initial3;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
   q= Q4(-1,-1,-1,pit3);
   dif=abs(pit3-q);
   pit3 = q;
      end
ntime3=toc
nsolution3 = pit3'

t=1e-10; %%tolerance level
pit4= initial4;
dif = 1; %difference between solutions of iterations
tic;

while abs(dif)>=t
   q= Q4(-1,-1,-1,pit4);
   dif=abs(pit4-q);
   pit4 = q;
      end
ntime4=toc
nsolution4 = pit4'
%It converges faster than the other two methods on first two initial guesses but is slower in other two%

%%
%Question 5
vc=[-4 -3 -2 -1 0 1];
initial=[1; 1; 1]; 
data=zeros(6,4);
for i=1:6
    q5=@(p) FOC(-1,-1,vc(i),p);   %take values as given, function of p only
    sol=broyden(q5,initial);
    data(i,:)=[sol',vc(i)];
end

plot(vc, data(:,1),vc,data(:,2),vc, data(:,3));
legend('pa','pb','pc')
    
    







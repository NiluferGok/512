clc
clear

load hw3.mat
beta0= zeros (6 , 1 ) ;


%% FMINUNC without derivative

options1 = optimset ('Display','iter','HessUpdate','bfgs') ;

betahat1= fminunc (@(b) loglikenew(X, y ,b), beta0, options1 ) ;
betahat1

%% FMINUNC with derivative
options2 = optimset ('Display','iter','gradobj','on','HessUpdate','bfgs') ;
% you need to increase defalut Fun tolarence here, otherwide your solution
% is different
betahat2= fminunc (@(b) loglikenew(X, y ,b), beta0, options2 ) ;
betahat2

%% Nelder-Mead
options3= optimset('Display','iter');

betahatn= fminsearch(@(b) loglikenew(X,y,b), beta0,options3);
betahatn

% here default value of 'MaxFunEval' is bindong. you needed to increase it
%% BHHH method
tol = 1e-10;
betahatbhhh= BHHH(X, y , beta0 ,tol) ;
betahatbhhh

%% Question 2
eig1=eig(hessnew(X,beta0))
eig2=eig(hessnew(X,betahatbhhh))

%% Question 3
options4 = optimoptions('lsqnonlin','Jacobian','on','Display','iter');
betahatnlls=lsqnonlin( @(b) nonlin(X, y ,b), beta0,[],[], options4);
betahatnlls




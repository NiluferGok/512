function EV = EV(V0,w)
load parameters.mat;
EV = zeros(3,1);

%Expectation of the value function when q1=0 and q2=0
%Given w, wprime can take four different values
EV1 = [V0(w(1),w(2)), V0(max(w(1)-1,1),w(2));V0(w(1),max(w(2)-1,1)), V0(max(w(1)-1,1),max(w(2)-1,1))];
Pr1 = kron(Prob(w(1),0)',Prob(w(2),0));
EV(1) = sum(sum(EV1.*Pr1));

% Expectation of the value function when q1=1 and q2=0
EV2 = [V0(min(w(1)+1,L),w(2)), V0(w(1),w(2)); V0(min(w(1)+1,L),max(w(2)-1,1)), V0(w(1),max(w(2)-1,1))];
Pr2 = kron(Prob(w(1),1)',Prob(w(2),0));
EV(2) = sum(sum(EV2.*Pr2));

%Expectation of the value function when q1=0 and q2=1
EV3=[V0(w(1),min(w(2)+1,L)),V0(max(w(1)-1,1),min(w(2)+1,L));
            V0(w(1),w(2)) ,V0(max(w(1)-1,1),w(2))];
Pr3= kron(Prob(w(1),0)',Prob(w(2),1));
EV(3) = sum(sum(EV3.*Pr3));
end
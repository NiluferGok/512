function [loglike,loglikei] = lhdmontecarlomax(b0,u0,sigb,sigmau,sigmabu,g,data,GQ)
  
aux = nan(data.N,1);

mu = [b0; u0];
Sig =  [sigb, sigmabu;sigmabu, sigmau];

z=mvnrnd([0;0],[1 0;0 1],GQ); %bivariate random variables with mean 0 and variance 1
b=(mu*ones(1,GQ))'+z*chol(Sig); %bivariate random variables with mean mu and variance Sig
w=(1/GQ)*ones(1,GQ); %monte carlo weights
for i=1:data.N
    x = data.X(:,i);
    y = data.Y(:,i);
    z = data.Z(:,i);
    
    aux(i) = w*ilhd(data.X(:,i),data.Y(:,i),data.Z(:,i),b(:,1),g,b(:,2));
end

loglikei = aux;
loglike = -sum(log(loglikei));

% Indiviual likelihood function for each observation
    function L_i = ilhd(x,y,z,beta,gamma,u)
        F = @(e) (1+exp(-e)).^(-1);
        arg = kron(beta',x) + kron(gamma*ones(size(beta')),z)+kron(u',ones(size(x)));
        L_i = prod(F(arg).^(kron(ones(size(beta')), y)).*(1-F(arg)).^(1 - kron(ones(size(beta')),y)))';
    end
end
function [loglike,loglikei] = lhd(b0,sigb,g,data,GQ)

%mu = [b0; 0];
%Sig =  [sigb, 0;0, 0];
  
aux = nan(data.N,1);

%[b,u] = qnwnorm([GQ GQ],mu,Sig); % bivariate normal 
[b,w]=qnwnorm(GQ,b0,sigb);% beta is distributed normally with mean b0 and variance sigb
for i=1:data.N
    x = data.X(:,i);
    y = data.Y(:,i);
    z = data.Z(:,i);
    
    aux(i) = w'*ilhd(data.X(:,i),data.Y(:,i),data.Z(:,i),b(:,1),g);
end

loglikei = aux;
loglike = -sum(log(loglikei));

% Indiviual likelihood function for each observation
    function L_i = ilhd(x,y,z,beta,gamma)
        F = @(e) (1+exp(-e)).^(-1);
        arg = kron(beta',x) + kron(gamma*ones(size(beta')),z);
        L_i = prod(F(arg).^(kron(ones(size(beta')), y)).*(1-F(arg)).^(1 - kron(ones(size(beta')),y)))';
    end
end
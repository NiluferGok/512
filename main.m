clc
clear
load parameters.mat
% Probability that firm makes a sale
D = @(p1,p2) exp(nu - p1)/(1 + exp(nu - p1) + exp(nu - p2));

%Marginal cost of production
c = @(W) kappa*W.^eta.*(W<l)+ kappa*l^eta.*(W>=l);


%initial guesses of value funtion,V and policy function, g
V0 = zeros(L);        
g0 = ones(L);        

g1 = nan(L);
V1 = nan(L);

eps = 1000;
maxiter = 20;

i = 1;                   
tol = 0.001;

while eps>tol && i<=maxiter
    for w1 = 1:L
        for w2 = 1:L
            P_ = g0(w2,w1); %other player's optimal decision
            w = [w1,w2];
            EVup= EV(V0,w);   
                  
            % FOC
            price = @(p) 1-(1-D(p,P_))*(p - c(w1))-beta*EVup(2)+beta*D(p,P_)*EVup(2)+beta*D(P_,p)*EVup(3)+... ...
                           + beta*((1 - D(p,P_) - D(P_,p))*EVup(1));
                       %where D(P_,p) is the selling probability of
                       %opponent firm and (1 - D(p,P_) - D(P_,p)) is the
                       %selling probability of the 0 firm
                    
            
            pup = broyden(price,10);  % To find the optimal price level given opponent's strategy
            g1(w1,w2) = pup;     %updating the price/policy function
            
            % Updating the value function
            V1(w1,w2) = D(pup,P_)*(pup - c(w1))+ beta*(D(pup,P_)*EVup(2)+ D(P_,pup)*EVup(3)...
                             + (1-D(pup,P_)-D(P_,pup))*EVup(1));
        end
    end
    
    % Dampening
    V1 = 0.5*V1 + (1 - 0.5)*V0; 
    g1 = 0.5*g1 + (1 - 0.5)*g0;
    
    eps1(i) = max(max(abs((V1 - V0)./(1 + V1))));
    eps2(i) = max(max(abs((g0 - g1)./(1 + g1))));
    eps = max(eps1(i),eps2(i));
    i = i+1;
    
    V0 = V1; 
    g0 = g1; 
end

save('main.mat','V1','g1')

figure(1)
mesh(V1) 
title('Value function')
xlabel('Firm 2 state')
ylabel('Firm 1 state')
zlabel('Value')

figure(2)
mesh(g1)
title('Policy function')
xlabel('Firm 2 state')
ylabel('Firm 1 state')
zlabel('Policy')

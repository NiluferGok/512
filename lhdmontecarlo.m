function [loglike,loglikei] = lhdmontecarlo(b0,sigb,g,data,GQ)
  
aux = nan(data.N,1);

b=b0+sigb*randn(GQ,1);
% Comment from TA: here you use the different draw each time you calculate
% likelihood. You should fix the seed, see answer key for that
w=(1/GQ)*ones(1,GQ); %monte carlo weights

for i=1:data.N
    x = data.X(:,i);
    y = data.Y(:,i);
    z = data.Z(:,i);
    
    aux(i) = w*ilhd(data.X(:,i),data.Y(:,i),data.Z(:,i),b(:,1),g);
end

loglikei = aux;
loglike = -sum(log(loglikei));

% Indiviual likelihood function for each observation
    function L_i = ilhd(x,y,z,beta,gamma)
        F = @(e) (1+exp(-e)).^(-1);
        arg = kron(beta',x) + kron(gamma*ones(size(beta')),z);
        L_i = prod(F(arg).^(kron(ones(size(beta')), y)).*(1-F(arg)).^(1 - kron(ones(size(beta')),y)))';
    end
end
function Y = q3( va, vb,vc,p );
qa= @(pa) (exp(va-pa))/(1+exp(va-pa)+exp(vb-p(2))+exp(vc-p(3)));

qb= @(pb) (exp(vb-pb))/(1+exp(va-p(1))+exp(vb-pb)+exp(vc-p(3)));

qc= @(pc) (exp(vc-pc))/(1+exp(va-p(1))+exp(vb-p(2))+exp(vc-pc));


FOC1=@(pa) qa(pa)-qa(pa)*(1-qa(pa))*pa;
FOC2=@(pb) qb(pb)-qb(pb)*(1-qb(pb))*pb;
FOC3=@(pc) qc(pc)-qc(pc)*(1-qc(pc))*pc;

% comment from TA you should sustitute previous subiteration solution into
% next subiteration, not using initial guesses
sol1=fsolve(FOC1,p(1));
sol2=fsolve(FOC2,p(2));
sol3=fsolve(FOC3,p(3));

Y=[sol1; sol2; sol3];

end

